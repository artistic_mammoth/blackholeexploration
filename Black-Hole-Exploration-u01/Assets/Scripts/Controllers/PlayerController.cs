using System;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float m_xDelta = 1.9f;
    private float m_speedDodge = 20f;
    private float m_newXPosition;
    private float m_backPosition;
    private float m_refilledX;
    private float m_refilledZ;
    private Line m_currentLine = Line.Middle;
    private CharacterController m_characterController;

    public delegate void OnHitObject(GameObject gObject);
    public static event OnHitObject OnHitObjectEvent;


    private void Awake()
    {
        m_characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        RunCharacter();
    }

    private void RunCharacter()
    {
        if(SwipeController.Instance.SWIPE_LEFT)
        {
            if(m_currentLine == Line.Middle)
            {
                m_newXPosition = -m_xDelta;
                m_currentLine = Line.Left;
            }
            else if (m_currentLine == Line.Right)
            {
                m_newXPosition = 0f;
                m_currentLine = Line.Middle;
            }
        }
        else if(SwipeController.Instance.SWIPE_RIGHT)
        {
            if(m_currentLine == Line.Middle)
            {
                m_newXPosition = m_xDelta;
                m_currentLine = Line.Right;
            }
            else if (m_currentLine == Line.Left)
            {
                m_newXPosition = 0f;
                m_currentLine = Line.Middle;
            }
        }
        else if (SwipeController.Instance.SWIPE_DOWN)
        {
            if (m_currentLine == Line.Left || m_currentLine == Line.Right)
            {
                m_newXPosition = 0f;
                m_currentLine = Line.Middle;
            }
            m_backPosition = 2.3f;
            StartCoroutine(BackMoveTimeOffset());
        }

        Vector3 moveVector = new Vector3(m_refilledX - transform.position.x, 0, m_refilledZ - transform.position.z);
        m_refilledX = Mathf.Lerp(m_refilledX, m_newXPosition, Time.fixedDeltaTime * m_speedDodge);
        m_refilledZ = Mathf.Lerp(m_refilledZ, m_backPosition, Time.fixedDeltaTime * m_speedDodge);
        m_characterController.Move(moveVector);

    }

    private void OnTriggerEnter(Collider other)
    {
        OnHitObjectEvent?.Invoke(other.gameObject);
    }

    private IEnumerator BackMoveTimeOffset()
    {
        yield return new WaitForSeconds(0.7f);
        m_backPosition = 0f;
    }

}

public enum Line
{
    Left,
    Middle,
    Right
}