using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    public static SwipeController Instance;

    private bool m_tap, m_swipeLeft, m_swipeRight, m_swipeDown, m_swipeUp;
    private bool m_isDraging;
    private Vector2 m_startTouch, m_swipeDelta;


    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }

    private void Update()
    {
        m_tap = m_swipeLeft = m_swipeRight = m_swipeDown = m_swipeUp = false;

        #region Standalone Input
        if (Input.GetMouseButtonDown(0))
        {
            m_tap = true;
            m_isDraging = true;
            m_startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            m_isDraging = false;
            ResetSwipe();
        }
        #endregion

        #region Mobile Input
        if (Input.touches.Length > 0)
        {
            if(Input.touches[0].phase == TouchPhase.Began)
            {
                m_tap = true;
                m_isDraging = true;
                m_startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                m_isDraging = false;
                ResetSwipe();
            }
        }
        #endregion

        m_swipeDelta = Vector2.zero;
        if (m_isDraging)
        {
            if (Input.touches.Length < 0)
                m_swipeDelta = Input.touches[0].position - m_startTouch;
            else if (Input.GetMouseButton(0))
                m_swipeDelta = (Vector2)Input.mousePosition - m_startTouch;
        }

        if (m_swipeDelta.magnitude > 100)
        {
            float x = m_swipeDelta.x;
            float y = m_swipeDelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                    m_swipeLeft = true;
                else
                    m_swipeRight = true;
            }
            else
            {
                if (y < 0)
                    m_swipeDown = true;
                else
                    m_swipeUp = true;
            }

            ResetSwipe();
        }
    }

    private void ResetSwipe()
    {
        m_startTouch = m_swipeDelta = Vector2.zero;
        m_isDraging = false;
    }

    public bool TAP {get { return m_tap; }}
    public bool SWIPE_LEFT {get { return m_swipeLeft; }}
    public bool SWIPE_RIGHT {get { return m_swipeRight; }}
    public bool SWIPE_DOWN {get { return m_swipeDown; }}
    public bool SWIPE_UP {get { return m_swipeUp; }}

}
