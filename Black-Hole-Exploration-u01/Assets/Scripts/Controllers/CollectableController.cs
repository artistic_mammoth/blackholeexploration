using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    public CollectableType ThisCollectableType;
    public CrystalType ThisCrystalType;
}
