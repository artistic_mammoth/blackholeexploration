using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private int m_lastCameraStep;
    private Transform m_camera;
    [SerializeField] private SpawnBiomesManager m_spawnBiomesManager;
    [SerializeField] private GameManager m_gameManager;
    private bool m_canMove = true;

    private void Awake()
    {
        m_camera = GetComponent<Transform>();
        CheckСurrentCamStep();
        m_lastCameraStep = DataKeeper.Instance.CURRENT_CAMERA_STEP;
    }

    private void FixedUpdate()
    {
        if (m_canMove)
            MoveCamera();

        if (DataKeeper.Instance.CURRENT_CAMERA_STEP != m_lastCameraStep)
        {
            m_lastCameraStep = DataKeeper.Instance.CURRENT_CAMERA_STEP;
            m_spawnBiomesManager.ReloadBiomes();
        }
    }

    private void MoveCamera()
    {
        m_camera.transform.position = Vector3.MoveTowards(m_camera.transform.position, m_camera.transform.position + Vector3.down, Time.deltaTime * m_gameManager.CurrentFallSpeed);
        CheckСurrentCamStep();
    }

    private void CheckСurrentCamStep()
    {
        DataKeeper.Instance.CURRENT_CAMERA_STEP = (int)(m_camera.transform.position.y / DataKeeper.Instance.CHUNK_LENGHT);
    }

}
