using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Obstacle", menuName = "BHE/Obstacle", order = 0)]
public class Obstacle : ScriptableObject
{
    public ObstacleType ObstacleType;
    public GameObject Prefab;
    public bool IsFlipAble;
    public bool Is2Pos;
    public bool Is2Side;
    public bool IsOnePos;
}

public enum ObstacleType
{
    Branch,
    Log,
    Bird,
    Spear,
    Waterfall
}
