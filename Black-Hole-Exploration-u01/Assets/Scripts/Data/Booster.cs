using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Booster", menuName = "BHE/Booster", order = 0)]
public class Booster : ScriptableObject
{
    public GameObject Prefab;
    public BoosterType BoosterType;
}

public enum BoosterType
{
    Shield,
    Freezer
}
