using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Collectable", menuName = "BHE/Collectable", order = 0)]
public class Collectable : ScriptableObject
{
    public GameObject Prefab;
    public CollectableType CollectableType;
    public CrystalType CrystalType;
    public int Ammount;

}

public enum CollectableType
{
    Crystal,
    Coin
}

public enum CrystalType
{
    White,
    Black
}