using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeStorage : MonoBehaviour
{
    public static BiomeStorage Instance;

    public Biome BiomeStartType;
    public Biome BiomeBlueType;
    public Biome BiomeRedType;

    private void Start()
    {
        BiomeStartType.Init();
        BiomeBlueType.Init();
        BiomeRedType.Init();
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
        GameObject.DontDestroyOnLoad(this.gameObject);
    }

}
