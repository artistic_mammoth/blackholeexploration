using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataKeeper : MonoBehaviour
{
    public static DataKeeper Instance;

    public int CHUNK_LENGHT = 20;
    public int CURRENT_CAMERA_STEP;
    public float CHUNK_COUNTER = 0;


    public float CURRENT_SCORE = 0;
    public float CURRENT_STYLE_SCORE = 0;
    public int TOTAL_STYLE_SCORE;
    public int TOTAL_SCORE;
    public float SCORE_SPEED_MULTIPLY = 1f;

    public float SCORE_PER_SECOND = 10f;
    public int CRYSTAL_AMOUNT;
    public float[] X_ABLE_POSITION = { -2.1f, 0f, 2.1f };
    public float[] X_ABLE_POSITION_FOR_OBSTACLES = { -2.5f, 0, 2.5f };
    public float[] X_ABLE_POSITION_FOR_2SIDE_OBSTACLE = { -1.5f, 1.5f };

    public bool IS_PAUSE = false;

    public bool BOOSTER_UNDER_SHIELD = false;
    public float BOOSTER_SHIELD_DURATION = 5f;
    public int BOOSTER_SHIELD_SAFE_COUNT = 1;
    public int BOOSTER_SHIELD_CURRENT_SAFE_COUNT = 0;

    public bool BOOSTER_IS_FREEZING;
    public float BOOSTER_FREEZE_AMOUNT = 0.5f;
    public float BOOSTER_FREEZE_DURATION = 2f;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
        GameObject.DontDestroyOnLoad(this.gameObject);
    }
}

