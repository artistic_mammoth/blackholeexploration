using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Biome", menuName = "BHE/Biome", order = 0)]
public class Biome : ScriptableObject 
{
    [SerializeField] private int MyIndex;
    public string Name;
    public List<Chunk> Chunks;
    public List<BiomeType> ForWhatBiomesCanChange;

    public List<int> ProbabilityList;

    public void Init()
    {
        BuildProbabilityList();
    }

    private void BuildProbabilityList()
    {
        if (ProbabilityList.Count != 0)
            return;

        int index = 0;
        foreach (Chunk chunk in Chunks)
        {
            for (int i = 0; i < chunk.Probability; i++)
                ProbabilityList.Add(index);
            index++;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

public enum BiomeType
{
    Start,
    Blue,
    Red
}