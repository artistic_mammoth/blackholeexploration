﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Chunk", menuName = "BHE/Chunk", order = 0)]
public class Chunk : ScriptableObject
{
    [SerializeField] private int MyIndex;
    public string Name;
    public GameObject Prefab;
    public BiomeType BiomeType;

    public List<Obstacle> Obstacles;
    [Range(-8.5f,7f)]
    public List<float> GenerateObstaclesMinYCoordinates;
    [Range(-8.5f, 7f)]
    public List<float> GenerateObstaclesMaxYCoordinates;

    public List<Collectable> Collectables;
    [Range(-8.5f, 7f)]
    public List<float> GenerateCollectableMinYCoordinates;
    [Range(-8.5f, 7f)]
    public List<float> GenerateCollectableMaxYCoordinates;

    public List<Booster> Boosters;
    [Range(-8.5f, 7f)]
    public List<float> GenerateBoosterMinYCoordinates;
    [Range(-8.5f, 7f)]
    public List<float> GenerateBoosterMaxYCoordinates;

    public int Probability;
    public bool IsChangeable;
}

public enum GeneratedObjectsType
{
    Obstacle,
    Collectable,
    Booster
}