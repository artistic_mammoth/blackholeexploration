using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransitionManager : MonoBehaviour
{   
    #region Singleton
    private static SceneTransitionManager _sceneTransitionManager;
    public static SceneTransitionManager instance
    {
        get
        {
            if (_sceneTransitionManager == null)
                _sceneTransitionManager = FindObjectOfType<SceneTransitionManager>();

            return _sceneTransitionManager;
        }
    }
    #endregion
    
    public Image transitionScreen;

    private void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.buildIndex == 1) 
            StartCoroutine(FadeOut(1, 0));
    }

    public void FadeIn(float a, float b, float t)
    {
        Color newTransitionScreenColor = transitionScreen.color;
        a = Mathf.Lerp(a, b, t);
        newTransitionScreenColor.a = a;
        transitionScreen.color = newTransitionScreenColor;
    } 

    private IEnumerator FadeOut(float a, float b)
    {
        var infinitesimal = 0.0001;
        while (Mathf.Abs(a - b) > infinitesimal)
        {
            Color newTransitionScreenColor = transitionScreen.color;
            a = Mathf.Lerp(a, b, 4 * Time.deltaTime);
            newTransitionScreenColor.a = a < 0.001 ? 0 : a;
            transitionScreen.color = newTransitionScreenColor;
                 
            yield return null;
        }
    }
}
