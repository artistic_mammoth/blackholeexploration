using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneLoader
{
    public static async void LoadScene(int buildIndex, SceneTransitionManager sceneTransitionManager)
    {
        AsyncOperation levelToLoad = SceneManager.LoadSceneAsync(buildIndex);
        levelToLoad.allowSceneActivation = false;

        while (sceneTransitionManager.transitionScreen.color.a < 0.99f && levelToLoad.isDone == false)
        {
            await System.Threading.Tasks.Task.Yield();
        }

        levelToLoad.allowSceneActivation = true;
    }
}
