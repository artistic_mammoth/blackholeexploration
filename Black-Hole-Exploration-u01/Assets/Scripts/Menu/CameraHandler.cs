using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private bool _standaloneInput = true;

    private Camera m_mainCamera;
    [SerializeField] [Range(1, 32)] private int _nearClipPlaneValue = 1;
    
    private Vector3 m_firstTouch;
    private Vector3 m_lastTouch;
    private Vector3 m_movement;

    private float m_decelerationValue = 1.0f;
    private float m_lerpValue = 1.5f;

    private bool m_startTransition = false;
    private bool m_isReadyForTheLoadScene = false;
    private Vector3 m_positionToMove;
    private Vector3 m_targetOriginPosition;

    [SerializeField] private Bounds _cameraBounds;
    private float m_originDistance;

    private void Start()
    {
        m_mainCamera = GetComponent<Camera>();
        _cameraBounds.center = transform.position;
    }

    private void LateUpdate()
    {
        if (m_isReadyForTheLoadScene == false)
        {
            Movement(); 

            if (m_startTransition == true)
                MoveToTarget(m_positionToMove);
        }
        else
        {
            LoadGameplay();
        }
    }

    private void Movement()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        mouseScreenPosition.z = m_mainCamera.nearClipPlane + _nearClipPlaneValue;
        Vector3 mouseWorldPosition = m_mainCamera.ScreenToWorldPoint(mouseScreenPosition);
        
        if (_standaloneInput)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_firstTouch = mouseWorldPosition;
                m_decelerationValue = 1.0f;
                m_startTransition = false;
            }
            else if (Input.GetMouseButton(0))
            {
                m_isReadyForTheLoadScene = false;
                m_lastTouch = mouseWorldPosition;
                m_movement = m_lastTouch - m_firstTouch;
                m_movement.y = 0;
                transform.Translate(-m_movement, Space.World);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                Ray rayFromCamera = m_mainCamera.ScreenPointToRay(mouseScreenPosition);
                if (Physics.Raycast(rayFromCamera, out RaycastHit hit, Mathf.Infinity))
                {
                    if (hit.collider.CompareTag("StartGameObj"))
                    {
                        m_startTransition = true;
                        m_targetOriginPosition = hit.collider.transform.position;
                        m_positionToMove = CalculateTargetTransitionPosition(m_targetOriginPosition);
                        if (Vector3.Distance(m_positionToMove, transform.position) == 0)
                            m_isReadyForTheLoadScene = true;
                    }
                }
            }
            else
            {
                if (m_movement.magnitude > 0)
                {
                    if (m_movement.magnitude < 0.0005d)
                        m_movement = Vector3.zero;

                    m_decelerationValue = Mathf.Lerp(m_decelerationValue, 0, m_lerpValue * Time.deltaTime);   
                }

                if (IsCameraInBounds() == false)
                    m_movement = Vector3.zero;
                else
                    transform.Translate(-m_movement * m_decelerationValue, Space.World);
                
                RecoverPosition();
            }
        }
        else if (Input.touchCount == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_firstTouch = mouseWorldPosition;
                m_decelerationValue = 1.0f;
                m_startTransition = false;
            }
            else if (Input.GetMouseButton(0))
            {
                m_isReadyForTheLoadScene = false;
                m_lastTouch = mouseWorldPosition;
                m_movement = m_lastTouch - m_firstTouch;
                m_movement.y = 0;
                transform.Translate(-m_movement, Space.World);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                Ray rayFromCamera = m_mainCamera.ScreenPointToRay(mouseScreenPosition);
                if (Physics.Raycast(rayFromCamera, out RaycastHit hit, Mathf.Infinity))
                {
                    if (hit.collider.CompareTag("StartGameObj"))
                    {
                        m_startTransition = true;
                        m_targetOriginPosition = hit.collider.transform.position;
                        m_positionToMove = CalculateTargetTransitionPosition(m_targetOriginPosition);
                        if (Vector3.Distance(m_positionToMove, transform.position) == 0)
                            m_isReadyForTheLoadScene = true;
                    }
                }
            }
        }
        else
        {
            if (m_movement.magnitude > 0)
            {
                if (m_movement.magnitude < 0.0005d)
                    m_movement = Vector3.zero;

                m_decelerationValue = Mathf.Lerp(m_decelerationValue, 0, m_lerpValue * Time.deltaTime);   
            }

            if (IsCameraInBounds() == false)
                m_movement = Vector3.zero;
            else
                transform.Translate(-m_movement * m_decelerationValue, Space.World);
            
            RecoverPosition();
        }
    }
    
    private Vector3 CalculateTargetTransitionPosition(Vector3 targetTransitionPosition)
    {   
        targetTransitionPosition.y = transform.position.y;
        targetTransitionPosition.z -= 3.75f;

        return targetTransitionPosition;
    }

    private void MoveToTarget(Vector3 targetPosition)
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, 50.0f * Time.deltaTime); 

        if (Vector3.Distance(targetPosition, transform.position) == 0)
            m_startTransition = false;
    }

    private bool IsCameraInBounds()
    {
        if ((transform.position.x <= _cameraBounds.max.x && transform.position.z <= _cameraBounds.max.z) && (transform.position.x >= _cameraBounds.min.x && transform.position.z >= _cameraBounds.min.z))
            return true;
        else 
            return false;
    }

    private void RecoverPosition()
    {
        if (IsCameraInBounds()) return;

        float xRecoveryPos = transform.position.x; 
        float zRecoveryPos = transform.position.z;

        if (transform.position.x > _cameraBounds.max.x)
            xRecoveryPos = _cameraBounds.max.x;
        else if (transform.position.x < _cameraBounds.min.x)
            xRecoveryPos = _cameraBounds.min.x;

        if (transform.position.z > _cameraBounds.max.z)
            zRecoveryPos = _cameraBounds.max.z;
        else if (transform.position.z < _cameraBounds.min.z)
            zRecoveryPos = _cameraBounds.min.z;
            
        Vector3 recoveryPos = new Vector3(xRecoveryPos, transform.position.y, zRecoveryPos);
        transform.position = Vector3.Lerp(transform.position, recoveryPos, 16 * Time.deltaTime); 
    }

    private void LoadGameplay()
    {
        if (transform.localEulerAngles.x >= 90.0f)
        {
            if (Vector3.Distance(transform.position, m_targetOriginPosition) == m_originDistance)
                SceneLoader.LoadScene(1, SceneTransitionManager.instance);
                
            transform.position = Vector3.Lerp(transform.position, m_targetOriginPosition, 8.0f * Time.deltaTime);
            float fadeValue = (-1/(m_originDistance / 2)) * transform.position.magnitude + 1;
            SceneTransitionManager.instance.FadeIn(0, 1, fadeValue);

            return;
        }

        Vector3 distance = m_targetOriginPosition - transform.position;
        m_originDistance = distance.magnitude;
        transform.localRotation = Quaternion.LookRotation(distance);

        Vector3 currentAngle = transform.localEulerAngles;
        currentAngle.x = Mathf.Lerp(currentAngle.x, 90.0f + 2, 2 * Time.deltaTime);
        transform.localEulerAngles = currentAngle;

        transform.position = new Vector3(transform.position.x, distance.magnitude * Mathf.Sin(transform.localEulerAngles.x * Mathf.Deg2Rad), distance.magnitude * -Mathf.Cos(transform.localEulerAngles.x * Mathf.Deg2Rad));
    }
}
