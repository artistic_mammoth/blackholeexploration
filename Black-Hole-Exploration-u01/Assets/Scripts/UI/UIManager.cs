using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameOverUI m_gameOverPanel;
    private float m_time = 0;
    [SerializeField] private Text m_timeText;
    private bool m_isInSession = false;

    private void OnEnable()
    {
        GameManager.EndGameEvent += OnEndGameUI;
    }

    private void OnDisable()
    {
        GameManager.EndGameEvent -= OnEndGameUI;
    }

    private void Start()
    {
        m_time = 0f;
        m_isInSession = true;
        StartCoroutine(TimeCountSession());
    }

    private void OnEndGameUI()
    {
        m_gameOverPanel.gameObject.SetActive(true);
        m_isInSession = false;
        m_gameOverPanel.StopSession(m_time);
    }

    private IEnumerator TimeCountSession()
    {
        while (m_isInSession)
        {
            m_time++;
            m_timeText.text = $"T: {m_time}";
            yield return new WaitForSeconds(1f);
        }
        yield break;
    }


}
