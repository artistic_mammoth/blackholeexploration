using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fpsCounter : MonoBehaviour
{
    public Text text;
    public int FPS;

    void Start()
    {
        StartCoroutine(Counter());
    }

    private IEnumerator Counter()
    {
        while (true)
        {
            FPS = (int)(1f / Time.deltaTime);
            text.text = "FPS: " + FPS;
            yield return new WaitForSeconds(1f);
        }
    }
}
