using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] private Text m_finalScoreText;
    [SerializeField] private Text m_finalStyleScoreText;
    [SerializeField] private Button m_restartButton;
    [SerializeField] private Text m_timeEndText;


    private void Start()
    {
        m_restartButton.onClick.AddListener(() => OnRestartButtonClick());
    }

    private void OnEnable()
    {
        UpdateScoreAndStyleUI();
    }

    private void OnRestartButtonClick()
    {
        DataKeeper.Instance.CURRENT_SCORE = 0f;
        DataKeeper.Instance.CURRENT_STYLE_SCORE = 0f;
        Time.timeScale = 1f;
        gameObject.SetActive(false);
        SceneManager.LoadScene("InGameScene");
    }

    private void UpdateScoreAndStyleUI()
    {
        m_finalScoreText.text = $"YOUR SCORE: {(int)DataKeeper.Instance.CURRENT_SCORE}";
        m_finalStyleScoreText.text = $"Your Style Score: {(int)DataKeeper.Instance.CURRENT_STYLE_SCORE}";
    }

    public void StopSession(float time)
    {
        m_timeEndText.text = $"T: {time}";
    }

}
