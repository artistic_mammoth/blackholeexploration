using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverlayUI : MonoBehaviour
{
    [SerializeField] private Text m_scoreText;
    [SerializeField] private Text m_crystalText;
    [SerializeField] private Button m_pauseButton;

    private float m_saveTimeScale = 1f;


    private void OnEnable()
    {
        GameManager.ScoreChangedEvent += ScoreUiUpdater;
        GameManager.CollectablePickUpEvent += CrystalUiUpdater;
    }

    private void OnDisable()
    {
        GameManager.ScoreChangedEvent -= ScoreUiUpdater;
        GameManager.CollectablePickUpEvent -= CrystalUiUpdater;
    }

    private void Start()
    {
        m_pauseButton.onClick.AddListener(() => OnPauseButtonClick());
        m_crystalText.text = $"Q: {DataKeeper.Instance.CRYSTAL_AMOUNT}";
    }

    private void ScoreUiUpdater(float score)
    {
        m_scoreText.text = $"Score: {(int)score}";
    }

    private void CrystalUiUpdater(CollectableType collectableType)
    {
        if (collectableType == CollectableType.Crystal)
        {
            m_crystalText.text = $"Q: {DataKeeper.Instance.CRYSTAL_AMOUNT}";
        }
    }

    private void OnPauseButtonClick()
    {
        if (DataKeeper.Instance.IS_PAUSE == false)
        {
            m_saveTimeScale = Time.timeScale;
            Time.timeScale = 0f;
        }
        else
            Time.timeScale = m_saveTimeScale;

        m_saveTimeScale = 1f;
        DataKeeper.Instance.IS_PAUSE = !DataKeeper.Instance.IS_PAUSE;
    }

}
