using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBiomesManager : MonoBehaviour
{
    private SpawnChunkManager m_spawnChunkManager;

    [SerializeField] private int m_drawDistance = 3;
    [SerializeField] private BiomeType m_currentBiomeType = BiomeType.Start;
    private Queue<GameObject> m_activeChunks;
    
    public delegate void NextChunkGenerated(float count);
    public static event NextChunkGenerated NextChunkGeneratedEvent;
    
    private void Awake()
    {
        m_spawnChunkManager = GetComponent<SpawnChunkManager>();
        m_activeChunks = new Queue<GameObject>();
        NextChunkGeneratedEvent?.Invoke(0f);
    }

    private void Start()
    {
        for (int i = 0; i < m_drawDistance; i++)
            SpawnBiomes();
    }

    public void ReloadBiomes()
    {
        DespawnOldChunk();
        SpawnBiomes();

        DataKeeper.Instance.CHUNK_COUNTER++;
        NextChunkGeneratedEvent?.Invoke(DataKeeper.Instance.CHUNK_COUNTER);
    }

    private void SpawnBiomes()
    {
        switch (m_currentBiomeType)
        {
            case BiomeType.Start: SpawnNewChunk(BiomeStorage.Instance.BiomeStartType); break;
            case BiomeType.Blue: SpawnNewChunk(BiomeStorage.Instance.BiomeBlueType); break;
            case BiomeType.Red: SpawnNewChunk(BiomeStorage.Instance.BiomeRedType); break;
            default: break;
        }
    }

    private void SpawnNewChunk(Biome biome)
    {
        int chunkIndex = biome.ProbabilityList[Random.Range(0, biome.ProbabilityList.Count)];
        GameObject newChunk = ObjectPool.Instance.GetObject(biome.Chunks[chunkIndex].Prefab);
        newChunk.transform.position = new Vector3(0f, (DataKeeper.Instance.CURRENT_CAMERA_STEP - m_activeChunks.Count) * DataKeeper.Instance.CHUNK_LENGHT, 0f);
        m_activeChunks.Enqueue(newChunk);
        newChunk.transform.SetParent(transform);

        if (biome.Chunks[chunkIndex].IsChangeable)
        {
            int indexType = Random.Range(0, biome.ForWhatBiomesCanChange.Count);
            m_currentBiomeType = biome.ForWhatBiomesCanChange[indexType];
        }

        m_spawnChunkManager.GenerateChunk(newChunk, biome.Chunks[chunkIndex]);
    }

    private void DespawnOldChunk()
    {
        GameObject oldChunk = m_activeChunks.Dequeue();
        oldChunk.SetActive(false);
    }

}
