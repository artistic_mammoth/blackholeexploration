using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnChunkManager : MonoBehaviour
{
    public void GenerateChunk(GameObject gObject, Chunk chunk)
    {
        GenerateChunkObstacles(gObject, chunk);
        GenerateChunkCollectables(gObject, chunk);
        GenerateChunkBoosters(gObject, chunk);
    }

    private void GenerateChunkObstacles(GameObject gObject, Chunk chunk)
    {
        for (int i = 0; i < chunk.Obstacles.Count; i++)
        {
            float yPos = Random.Range(chunk.GenerateObstaclesMinYCoordinates[i], chunk.GenerateObstaclesMaxYCoordinates[i]);
            GameObject newObstacle = ObjectPool.Instance.GetObject(chunk.Obstacles[i].Prefab);
            newObstacle.transform.SetParent(gObject.transform);
            newObstacle.transform.rotation = chunk.Obstacles[i].Prefab.transform.rotation;
            float xPos = 0f;
            float zPos = 0f;
            
            if (chunk.Obstacles[i].IsFlipAble)
            {
                xPos = GenerateRandomXAblePosiiton(GeneratedObjectsType.Obstacle, chunk.Obstacles[i].Is2Side, chunk.Obstacles[i].Is2Pos);

                if (xPos == DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[2])
                {
                    newObstacle.transform.Rotate(0f, (newObstacle.transform.rotation.y + 180f), 0f, Space.World);
                    //newObstacle.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else if (xPos == DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[1])
                {
                    newObstacle.transform.Rotate(0f, (newObstacle.transform.rotation.y + 90f), 0f, Space.World);
                    //newObstacle.transform.localScale = new Vector3(2f, 1f, 1f);
                    zPos += 1f;
                }
                else if (xPos == DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[0])
                {
                    //newObstacle.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else if(xPos == DataKeeper.Instance.X_ABLE_POSITION_FOR_2SIDE_OBSTACLE[1])
                {
                    newObstacle.transform.Rotate(0f, (newObstacle.transform.rotation.y + 180f), 0f, Space.World);
                }

            }

            newObstacle.transform.localPosition = new Vector3(xPos, yPos, zPos);

            ObstacleController obstacleController = newObstacle.GetComponent<ObstacleController>();
            if (obstacleController == null)
                obstacleController = newObstacle.AddComponent<ObstacleController>();
            obstacleController.ThisObstacleType = chunk.Obstacles[i].ObstacleType;
        }
    }

    private void GenerateChunkCollectables(GameObject gObject, Chunk chunk)
    {
        for (int i = 0; i < chunk.Collectables.Count; i++)
        {
            float yPos = Random.Range(chunk.GenerateCollectableMinYCoordinates[i], chunk.GenerateCollectableMaxYCoordinates[i]);
            GameObject newCollectable = ObjectPool.Instance.GetObject(chunk.Collectables[i].Prefab);
            newCollectable.transform.rotation = chunk.Collectables[i].Prefab.transform.rotation;
            newCollectable.transform.SetParent(gObject.transform);
            float xPos = GenerateRandomXAblePosiiton(GeneratedObjectsType.Collectable);
            newCollectable.transform.localPosition = new Vector2(xPos, yPos);

            CollectableController collectableController = newCollectable.GetComponent<CollectableController>();
            if (collectableController == null)
                collectableController = newCollectable.AddComponent<CollectableController>();
            collectableController.ThisCollectableType = chunk.Collectables[i].CollectableType;
            collectableController.ThisCrystalType = chunk.Collectables[i].CrystalType;

        }
    }

    private void GenerateChunkBoosters(GameObject gObject, Chunk chunk)
    {
        if (DataKeeper.Instance.BOOSTER_UNDER_SHIELD)
            return;
        if (!CanSpawnBoosterChance())
            return;

        for (int i = 0; i < chunk.Boosters.Count; i++)
        {
            float yPos = Random.Range(chunk.GenerateBoosterMinYCoordinates[i], chunk.GenerateBoosterMaxYCoordinates[i]);
            GameObject newBooster = ObjectPool.Instance.GetObject(chunk.Boosters[i].Prefab);
            newBooster.transform.rotation = chunk.Boosters[i].Prefab.transform.rotation;
            newBooster.transform.SetParent(gObject.transform);
            float xPos = GenerateRandomXAblePosiiton(GeneratedObjectsType.Booster);
            newBooster.transform.localPosition = new Vector2(xPos, yPos);

            BoosterController boosterController = newBooster.GetComponent<BoosterController>();
            if (boosterController == null)
                boosterController = newBooster.AddComponent<BoosterController>();
            boosterController.ThisBoosterType = chunk.Boosters[i].BoosterType;
        }
    }

    private float GenerateRandomXAblePosiiton(GeneratedObjectsType generatedObjectsType, bool is2Side = false, bool is2Pos = false)
    {
        float value = 0f;
        if (generatedObjectsType == GeneratedObjectsType.Obstacle)
            value = DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[Random.Range(0, DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES.Length)];
        else if (generatedObjectsType == GeneratedObjectsType.Collectable || generatedObjectsType == GeneratedObjectsType.Booster)
            value = DataKeeper.Instance.X_ABLE_POSITION[Random.Range(0, DataKeeper.Instance.X_ABLE_POSITION.Length)];

        if (is2Side)
            value = DataKeeper.Instance.X_ABLE_POSITION_FOR_2SIDE_OBSTACLE[Random.Range(0, DataKeeper.Instance.X_ABLE_POSITION_FOR_2SIDE_OBSTACLE.Length)];

        if (is2Pos)
            while (value == DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[1])
                value = DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[Random.Range(0, DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES.Length)];

        return value;
    }

    private int GetRandomChanceForBoosterSpawn()
    {
        int value = 0;

        value = Mathf.RoundToInt(Random.Range(0f, 100f));

        return value;
    }

    private bool CanSpawnBoosterChance()
    {
        int percent = GetRandomChanceForBoosterSpawn();
        bool value = false;
        if (percent <= 70)
            value = true;
        return value;
    }
}
