using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float CurrentFallSpeed { get { return m_currentFallSpeed; } private set { } }

    [SerializeField] private float m_currentFallSpeed = 8f;
    [SerializeField] private float m_basicFallSpeed = 8f;
    [SerializeField] private GameObject m_vfxCrystal;
    [SerializeField] CameraController m_cameraController;


    public delegate void ScoreChanged(float score);
    public static event ScoreChanged ScoreChangedEvent;

    public delegate void CollectablePickUp(CollectableType collectableType);
    public static event CollectablePickUp CollectablePickUpEvent;

    public delegate void EndGame();
    public static event EndGame EndGameEvent;


    private void Awake()
    {
        ScoreChangedEvent?.Invoke(0f);
    }

    private void OnEnable()
    {
        PlayerController.OnHitObjectEvent += OnHit;
        SpawnBiomesManager.NextChunkGeneratedEvent += CheckGloabalMultiples;
    }

    private void OnDisable()
    {
        PlayerController.OnHitObjectEvent -= OnHit;
        SpawnBiomesManager.NextChunkGeneratedEvent -= CheckGloabalMultiples;
    }

    private void Start()
    {
        StartCoroutine(ScoreIncreser());
    }

    private void OnHit(GameObject gObject)
    {
        if (gObject.tag == "Obstacle")
        {
            if (DataKeeper.Instance.BOOSTER_UNDER_SHIELD)
            {
                DataKeeper.Instance.BOOSTER_SHIELD_CURRENT_SAFE_COUNT--;
                return;
            }

            OnDead();
        }
        else if (gObject.tag == "Collectable")
        {
            OnPickUpCollectable(gObject);
        }
        else if (gObject.tag == "Booster")
        {
            UseBooster(gObject);
        }

    }

    private void UseBooster(GameObject gObject)
    {
        BoosterController boosterController = gObject.GetComponent<BoosterController>();

        if (boosterController != null)
        {
            BoosterType boosterType = boosterController.ThisBoosterType;

            switch (boosterType)
            {
                case BoosterType.Shield: OnShield(); break;
                case BoosterType.Freezer: OnFreeze();  break;
                default: break;
            }
        }
        gObject.SetActive(false);

    }

    private void OnDead()
    {
        CalculateStyleScoreOnEndGame();
        DataKeeper.Instance.TOTAL_SCORE += Mathf.RoundToInt(DataKeeper.Instance.CURRENT_SCORE);
        DataKeeper.Instance.TOTAL_STYLE_SCORE += Mathf.RoundToInt(DataKeeper.Instance.CURRENT_STYLE_SCORE);
        EndGameEvent?.Invoke();
        Time.timeScale = 0f;
    }

    private void OnPickUpCollectable(GameObject gObject)
    {
        CollectableController collectableController =  gObject.GetComponent<CollectableController>();

        if (collectableController != null)
        {
            if (collectableController.ThisCollectableType == CollectableType.Crystal)
            {
                DataKeeper.Instance.CRYSTAL_AMOUNT++;
                GameObject vfx = ObjectPool.Instance.GetObject(m_vfxCrystal);
                vfx.transform.position = gObject.transform.position;
                StartCoroutine(FalseVfx(vfx));
                gObject.SetActive(false);
            }
        }
        CollectablePickUpEvent?.Invoke(collectableController.ThisCollectableType);
    }

    private void OnShield()
    {
        if (!DataKeeper.Instance.BOOSTER_UNDER_SHIELD)
            StartCoroutine(DoShield(DataKeeper.Instance.BOOSTER_SHIELD_DURATION));
        else
            Debug.Log("<color=blue> Already UnderShield </color>");
    }

    private IEnumerator DoShield(float shieldDuration)
    {
        DataKeeper.Instance.BOOSTER_UNDER_SHIELD = true;
        DataKeeper.Instance.BOOSTER_SHIELD_CURRENT_SAFE_COUNT = DataKeeper.Instance.BOOSTER_SHIELD_SAFE_COUNT;
        while (shieldDuration >= 0)
        {
            if (DataKeeper.Instance.BOOSTER_SHIELD_CURRENT_SAFE_COUNT <= 0)
            {
                BreakShield();
                yield break;
            }
            else
            {
                Debug.Log("shieldDuration " + shieldDuration);
                shieldDuration--;
                yield return new WaitForSecondsRealtime(1f);
            }
        }
        BreakShield();
        yield break;
    }

    private void BreakShield()
    {
        DataKeeper.Instance.BOOSTER_UNDER_SHIELD = false;
        Debug.Log("Shield is Broken");
    }


    private void OnFreeze()
    {
        if (!DataKeeper.Instance.BOOSTER_IS_FREEZING)
            StartCoroutine(DoFreeze());
        else
            Debug.Log("<color=blue> Already IsFreezing </color>");
    }

    private IEnumerator DoFreeze()
    {
        DataKeeper.Instance.BOOSTER_IS_FREEZING = true;
        var originalTimeScale = Time.timeScale;
        Time.timeScale = DataKeeper.Instance.BOOSTER_FREEZE_AMOUNT;
        yield return new WaitForSecondsRealtime(DataKeeper.Instance.BOOSTER_FREEZE_DURATION);
        Time.timeScale = originalTimeScale;
        DataKeeper.Instance.BOOSTER_IS_FREEZING = false;
    }


    private void CalculateStyleScoreOnEndGame()
    {
        DataKeeper.Instance.CURRENT_STYLE_SCORE = Mathf.RoundToInt(DataKeeper.Instance.CURRENT_SCORE * 0.3f);
    }

    private void CheckGloabalMultiples(float chunkCounter)
    {
        CheckSpeedMultiply();
        CheckGlobalSpeed();
    }

    private void CheckSpeedMultiply()
    {
        if (DataKeeper.Instance.CHUNK_COUNTER > 0 && DataKeeper.Instance.SCORE_SPEED_MULTIPLY < 2.7f)
            DataKeeper.Instance.SCORE_SPEED_MULTIPLY += DataKeeper.Instance.CHUNK_COUNTER / 10;
        else if (DataKeeper.Instance.SCORE_SPEED_MULTIPLY >= 2.7f)
            DataKeeper.Instance.SCORE_SPEED_MULTIPLY = 2.7f;
        else
            DataKeeper.Instance.SCORE_SPEED_MULTIPLY = 1f;
    }

    private void CheckGlobalSpeed()
    {
        if (DataKeeper.Instance.CHUNK_COUNTER > 0 && m_currentFallSpeed < 11f)
            m_currentFallSpeed = m_basicFallSpeed + (DataKeeper.Instance.CHUNK_COUNTER / 15);
        else if (m_currentFallSpeed >= 11f)
            m_currentFallSpeed = 11f;
        else
            m_currentFallSpeed = m_basicFallSpeed;
    }

    private IEnumerator ScoreIncreser()
    {
        while (true)
        {     
            DataKeeper.Instance.CURRENT_SCORE += (DataKeeper.Instance.SCORE_PER_SECOND * DataKeeper.Instance.SCORE_SPEED_MULTIPLY) / 12.5f;
            ScoreChangedEvent?.Invoke(DataKeeper.Instance.CURRENT_SCORE);
            yield return new WaitForSeconds(0.08f);
        }
    }

    private IEnumerator FalseVfx(GameObject gObject)
    {
        yield return new WaitForSeconds(1f);
        gObject.SetActive(false);
    }

}
