using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    [SerializeField] private bool m_facingRight = true;
    [SerializeField] private Vector3 m_dir = Vector3.right;
    [SerializeField] private float m_speed = 2f;




    private void Update()
    {
        if (transform.position.x >= DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[2])
            m_dir = Vector3.left;
        else if (transform.position.x <= DataKeeper.Instance.X_ABLE_POSITION_FOR_OBSTACLES[0])
            m_dir = Vector3.right;


        transform.position = Vector3.MoveTowards(transform.position, transform.position + m_dir, Time.deltaTime * m_speed);

        if ((m_dir.x > 0 && !m_facingRight) || (m_dir.x < 0 && m_facingRight))
            Flip();
    }

    private void Flip()
    {
        m_facingRight = !m_facingRight;
        transform.rotation = Quaternion.Euler(0, m_facingRight ? 0 : 180, 0);
    }
}
